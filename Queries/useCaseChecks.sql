--PRAGMA foreign_keys=on;
--use case 1:
--top 10 dishes from 1850 to 2012
select name,sum(times_appeared) as times_appeared from raw_dish
where ((substr(first_appeared,1,4)  between '1851' and '2012') or (substr(last_appeared,1,4)  between '1851' and '2012'))
group by name order by times_appeared desc


select name,sum(times_appeared) as times_appeared from cleaned_dish
where ((substr(first_appeared,1,4)  between '1851' and '2012') or (substr(last_appeared,1,4)  between '1851' and '2012'))
group by name order by times_appeared desc


--use case 2

--which variety of coffee beverage has appeared max times between '1851' and '2012'
select name,sum(times_appeared) as times_appeared from raw_dish
where ((substr(first_appeared,1,4)  between '1851' and '2012') or (substr(last_appeared,1,4)  between '1851' and '2012'))
and upper(name) like '%COFFEE%'
group by name order by times_appeared desc

select name,sum(times_appeared) as times_appeared from cleaned_dish
where ((substr(first_appeared,1,4)  between '1851' and '2012') or (substr(last_appeared,1,4)  between '1851' and '2012'))
and name like '%COFFEE%'
group by name order by times_appeared desc

--use case 3

select venue,d.name,count(*) as cnt from cleaned_menu a, cleaned_dish d ,cleaned_menuitem c, cleaned_menupage b
where a.id = b.menu_id
and b.id= c.menu_page_id
and c.dish_id = d.id
and first_appeared<>1
and cast((substr(last_appeared,1,4)) as int) > 2010
and cast((substr(last_appeared,1,4)) as int) < 2021
and (place like '%NY' or place like  'NEW YORK')
group by venue,d.name
order by cnt desc limit 20



select venue,d.name,count(*) as cnt from raw_menu a, raw_dish d ,raw_menuitem c, raw_menupage b
where a.id = b.menu_id
and b.id= c.menu_page_id
and c.dish_id = d.id
and first_appeared<>1
and last_appeared > 2010
and last_appeared < 2021
and (place like '%NY' or place like  'NEW YORK')
group by venue,d.name
order by cnt desc limit 20

-->low priority ones

--case U0

--what dishes appeared last in 1927

select distinct name  from raw_dish where last_appeared =1927

--What is the max full height and max full width of the menupages

select max(full_height)  as max_full_height from raw_menupage where full_height is null or full_height <>''
select max(full_width)  as max_full_width from raw_menupage where full_width is null or full_width <>''

--which menu id has the maximum pages  and how much in the decreasing order
select menu_id,max(page_number)  as max_value from raw_menupage  where page_number <>'' group by menu_id order by max_value desc

--Menu
--currency_symbol 	4
--date_clean 	0
--dish_count 	0
--event 	1363
--keywords 	0
--language 	0
--location 	15885
--location_type 	0
--name 	3198
--notes 	125
--occasion 	2542
--page_count 	0
--physical_description 	12643
--place 	3637
--sponsor 	9793
--status 	15929
--venue 	1987
--
--Menupage
--full_height 	0
--full_width 	0
--image_id 	0
--menu_id 	0
--page_number 	0
--uuid 	0
--
--dish
--first_appeared  	339105
--highest_price 	0
--last_appeared  	339105
--lowest_price 	0
--menus_appeared 	4
--name 	328752
--times_appeared  	9
--
--menuitem
--id 193004 deleteed to satisfy constraints
--created_at 	0
--dish_id 	1
--high_price 	0
--menu_page_id 	0
--price 	0
--updated_at 	1
--xpos 	0
--ypos 	0